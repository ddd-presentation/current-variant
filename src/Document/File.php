<?php

namespace Document;

use MongoDB\BSON\ObjectId;

class File
{
    private ObjectId $id;
    private string $filePath;
    private string $fileExt;
    private int $fileSize;

    public function __construct(array $file) {
        $this->id = $file['id'] ?? new ObjectId();
        $this->fileSize = $file['size'];
        $this->filePath = $file['path'];
        $this->fileExt = $file['ext'];
    }

    public function getId(): ObjectId
    {
        return $this->id;
    }

    public function setFilePath(string $filePath) {
        $this->filePath = $filePath;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function setFileExt(string $fileExt) {
        $this->fileExt = $fileExt;
    }

    public function getFileExt(): string
    {
        return $this->fileExt;
    }

    public function setFileSize(int $fileSize) {
        $this->fileSize = $fileSize;
    }

    public function getFileSize(): int
    {
        return $this->fileSize;
    }
}