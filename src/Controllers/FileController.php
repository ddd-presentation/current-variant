<?php

use Files\FileManager;

class FileController
{
    private FileManager $fileManager;

    public function __construct(
        FileManager $fileManager
    ) {
        $this->fileManager = $fileManager;
    }

    /**
     * @Route()
     * @throws Exception
     */
    public function putFile(Request $request): Response
    {
        /** @var array $file */
        $file = $request->getFiles();
        if (!$this->isFileSizeLessThen5mb($file)) {
            throw new \Exception();
        }

        if (!$this->isFileExtPds($file)) {
            throw new \Exception();
        }

        $this->fileManager->saveFile($file);

        return new Response();
    }

    /** @Route() */
    public function getFiles(Request $request): Response
    {
        $files = $this->fileManager->getFiles();

        return new Response($files);
    }

    private function isFileSizeLessThen5mb(array $file): bool
    {
        /**
         * ...
         **/

        return false;
    }

    private function isFileExtPds(array $file): bool
    {
        /**
         * ...
         **/

        return false;
    }
}