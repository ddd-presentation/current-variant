<?php

namespace Files;

use Document\File;
use Document\FileRepository;
use S3\S3FilesProvider;

class FileManager
{
    private FileRepository $fileRepository;
    private S3FilesProvider $s3FilesProvider;

    public function __construct(FileRepository $fileRepository) {
        $this->fileRepository = $fileRepository;
    }

    public function saveFile(array $file)
    {
        $fileToPersist = new File($file);
        $this->s3FilesProvider->save($fileToPersist);
        $this->fileRepository->save(new File($file));
    }

    /**
     * @return File[]
     */
    public function getFiles(): array
    {
        $query = [
            'queryCondition' => 'queryCondition'
        ];

        return $this->fileRepository->get($query);
    }
}